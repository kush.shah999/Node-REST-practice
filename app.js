var express = require('express');
var routes = require('./routes');
var hostname = 'localhost';
var port = 3000;
var app =express();

app.use(express.static(__dirname + '/routes'));
app.use('/', routes);
app.listen(port,hostname,function(){
  console.log('Server running on port '+port);
});
