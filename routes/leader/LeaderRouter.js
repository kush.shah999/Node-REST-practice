var LeaderRouter = require('express').Router();
var fs = require('fs');
var bodyparser= require('body-parser');
LeaderRouter.use(bodyparser.json());
LeaderRouter.all('/', function(req,res,next){
  res.writeHead(200,{'Content-Type': 'text/plain'});
  next();
});
LeaderRouter.get('/',function(req,res,next){
  res.end('Will send the leader list to you!');
});
LeaderRouter.post('/',function(req,res,next){
  res.end('will add the leader named : '+req.body.name+' and the Description : '+req.body.description);
});
LeaderRouter.delete('/',function(req,res,next){
  res.end('will DELETE all leaderes');
});
LeaderRouter.get('/:leaderId',function(req,res,next){
  res.end('Will send the leader with name : '+req.params.leaderId+' to you!');
});
LeaderRouter.put('/:leaderId',function(req,res,next){
  res.end('Update the leader named : '+req.params.leaderId+', Details to : '+req.body.description);
});
LeaderRouter.delete('/:leaderId',function(req,res,next){
  res.end('will DELETE leader : '+req.params.leaderId);
});

module.exports = LeaderRouter;
