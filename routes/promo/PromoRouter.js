var PromoRouter = require('express').Router();
var fs = require('fs');
var bodyparser= require('body-parser');
PromoRouter.use(bodyparser.json());
PromoRouter.all('/', function(req,res,next){
  res.writeHead(200,{'Content-Type': 'text/plain'});
  next();
});
PromoRouter.get('/',function(req,res,next){
  res.end('Will send the promotions list to you!');
});
PromoRouter.post('/',function(req,res,next){
  res.end('will add the promotions named : '+req.body.name+' and the Description : '+req.body.description);
});
PromoRouter.delete('/',function(req,res,next){
  res.end('will DELETE all promotionses');
});
PromoRouter.get('/:promoId',function(req,res,next){
  res.end('Will send the promotions with name : '+req.params.promoId+' to you!');
});
PromoRouter.put('/:promoId',function(req,res,next){
  res.end('Update the promotions named : '+req.params.promoId+', Details to : '+req.body.description);
});
PromoRouter.delete('/:promoId',function(req,res,next){
  res.end('will DELETE promotions : '+req.params.promoId);
});

module.exports = PromoRouter;
