var DishRouter = require('express').Router();
var fs = require('fs');
var bodyparser= require('body-parser');
DishRouter.use(bodyparser.json());
DishRouter.all('/', function(req,res,next){
  res.writeHead(200,{'Content-Type': 'text/plain'});
  next();
});
DishRouter.get('/',function(req,res,next){
  res.end('Will send the dish list to you!');
});
DishRouter.post('/',function(req,res,next){
  res.end('will add the Dish named : '+req.body.name+' and the Description : '+req.body.description);
});
DishRouter.delete('/',function(req,res,next){
  res.end('will DELETE all dishes');
});
DishRouter.get('/:dishId',function(req,res,next){
  res.end('Will send the dish with name : '+req.params.dishId+' to you!');
});
DishRouter.put('/:dishId',function(req,res,next){
  res.end('Update the dish named : '+req.params.dishId+', Details to : '+req.body.description);
});
DishRouter.delete('/:dishId',function(req,res,next){
  res.end('will DELETE dish : '+req.params.dishId);
});

module.exports = DishRouter;
