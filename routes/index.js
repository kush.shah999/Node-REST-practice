var express = require('express');
var fs = require('fs');
var dishRouter = require('./dish/DishRouter');
var promoRouter = require('./promo/PromoRouter');
var leaderRouter = require('./leader/LeaderRouter');

var routes= express.Router();
var util = require('util');
routes.get('/', (req, res, next) => {
  console.log(util.inspect(req));
  res.writeHead(200,{'Content-Type': 'text/plain'});
  next();
});
routes.delete('/',(req,res)=> {
  res.sendStatus(404);
});
routes.use('/dishes', dishRouter);
routes.use('/leader', leaderRouter);
routes.use('/promo', promoRouter);
module.exports = routes;
